package edu.upenn.cit594.processor;

import java.util.ArrayList;

public class FindingState {
	
	
	public ArrayList<String> ListState(ArrayList<String> Location, ArrayList<String> latitude, ArrayList<String> longitude, ArrayList<String> state) {
		
		ArrayList<Double> xCoord = new ArrayList<>();
		ArrayList<Double> yCoord = new ArrayList<>();
		ArrayList<String> stateTweets = new ArrayList<>();
		ArrayList<Double> xCoordS = new ArrayList<>();
		ArrayList<Double> yCoordS = new ArrayList<>();				
		
		for (int i = 0; i < Location.size(); i++) {
			Double lat = Double.parseDouble(Location.get(i).substring(Location.get(i).indexOf("[")+1,Location.get(i).indexOf(",")));
			Double longi = Double.parseDouble(Location.get(i).substring(Location.get(i).indexOf(",")+1,Location.get(i).indexOf("]")));
			xCoord.add(lat);
			yCoord.add(longi);
			
		}
		
		for (int j = 0; j < latitude.size(); j++) {
			xCoordS.add(Double.parseDouble(latitude.get(j)));			
		}
		
		for (int k = 0; k < longitude.size(); k++) {
			yCoordS.add(Double.parseDouble(longitude.get(k)));			
		}
		
		for (int i = 0; i < Location.size(); i++) {
			String closestState = null;
			double distance = 0;
			for (int j = 0; j < latitude.size(); j++) {
				double xsquare = (xCoord.get(i)-xCoordS.get(j))*(xCoord.get(i)-xCoordS.get(j));
				double ysquare = (yCoord.get(i)-yCoordS.get(j))*(yCoord.get(i)-yCoordS.get(j));
				double distance_tracker = Math.sqrt(xsquare+ysquare);				
				if (distance == 0) {
					distance = distance_tracker;
					closestState = state.get(j);
				}
				else if (distance_tracker < distance) {
					distance = distance_tracker;
					closestState = state.get(j);
				}
				else {					
				}							
			}
			stateTweets.add(closestState);
				
		}
		return stateTweets;

}
}
