package edu.upenn.cit594.processor;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import edu.upenn.cit594.logging.Loggers;

public class FluFinder {
	
	private static boolean isContain(String source){
        String pattern = "\\b"+"flu"+"\\b";
        Pattern p=Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m=p.matcher(source);
        return m.find();
   }
	
	public ArrayList<Boolean> FluTweet(ArrayList<String> Text, ArrayList<String> closestState){
		
		ArrayList<Boolean> hasFlu = new ArrayList<Boolean>();

		for (int i = 0; i < Text.size(); i++) {
			String line = Text.get(i);
			String lineFromatted  = line.replaceAll("[^a-zA-Z0-9]", " ");
			hasFlu.add(isContain(lineFromatted));
			Loggers newLogger = Loggers.getInstance();
			if (isContain(lineFromatted)) {
				newLogger.log(closestState.get(i)+ "\t" + line);				
			}
		}
		
		return hasFlu;
						
	}

}
