package edu.upenn.cit594.processor;
import java.io.IOException;

import java.util.*;
import edu.upenn.cit594.datamanagement.*;
import edu.upenn.cit594.data.*;

public class Processor {

	private MainReader fileReader;
	private MainReader CSVReader;
	private List<Tweets> tweets;
	private List<csvStates> states;
	private ArrayList<String> Location;
	private ArrayList<String> Text;
	private ArrayList<String> latitude;
	private ArrayList<String> longitude;
	private ArrayList<String> state;
	private ArrayList<String> closestState;
	private ArrayList<Boolean> booleanTweets;
	private ArrayList<String> fluStates;	
	
	@SuppressWarnings("unchecked")
	public Processor(String format, String fileName, String statesName, String logFile) throws IOException {
		ReaderPicker rp = new ReaderPicker();
		this.fileReader = rp.getReader(format, fileName);//gets tweets
		this.CSVReader = rp.getCSVReader(statesName);//gets states
		tweets = (List<Tweets>) fileReader.read();
		states = (List<csvStates>) CSVReader.read();
	}
	
	public ArrayList<String> getLocationparsed(){
		Location = new ArrayList<String>();
		for (Tweets t : tweets) {
			Location.add(t.getLocation());
		}
		return Location;
	}
	
	public ArrayList<String> getTextparsed(){
		Text = new ArrayList<String>();
		for (Tweets t : tweets) {
			Text.add(t.getText());
		}
		return Text;
	}
	
	public ArrayList<String> getlatitudeparsed(){
		latitude = new ArrayList<String>();
		for (csvStates c : states) {
			latitude.add(c.getLat());
		}
		return latitude;
	}
	
	public ArrayList<String> getlongitudeparsed(){
		longitude = new ArrayList<String>();
		for (csvStates c : states) {
			longitude.add(c.getLongi());
		}
		return longitude;
	}
	
	public ArrayList<String> getstateparsed(){
		state = new ArrayList<String>();
		for (csvStates c : states) {
			state.add(c.getState());
		}
		return state;
	}
	

		/*
		latitude = new ArrayList<String>();
		longitude = new ArrayList<String>();
		state = new ArrayList<String>();
		for (csvStates c : states) {
			latitude.add(c.getLat());
			longitude.add(c.getLongi());
			state.add(c.getState());
		}*/
	
	public ArrayList<String> getclosestState(){
		closestState = new ArrayList<String>();
		FindingState listStates = new FindingState();
		this.closestState = listStates.ListState(Location, latitude, longitude, state);
		return closestState;
	}
		/*
		closestState = new ArrayList<String>();
		FindingState listStates = new FindingState();
		this.closestState = listStates.ListState(Location, latitude, longitude, state);
		booleanTweets = new ArrayList<Boolean>();
		FluFinder booleanTweet = new FluFinder();
		this.booleanTweets = booleanTweet.FluTweet(Text, closestState);
		*/

	public ArrayList<Boolean> getbooleanTweets(){
		booleanTweets = new ArrayList<Boolean>();
		FluFinder booleanTweet = new FluFinder();
		this.booleanTweets = booleanTweet.FluTweet(Text, closestState);
		return booleanTweets;
	}
	
	public ArrayList<String> getfluStates(){
		fluStates = new ArrayList<String>();
		for (int i = 0; i < booleanTweets.size(); i++) {			
			if (booleanTweets.get(i) == true) {
				fluStates.add(closestState.get(i));				
			}
		}
		return fluStates;
	}
		
		/*
		fluStates = new ArrayList<String>();
		for (int i = 0; i < booleanTweets.size(); i++) {			
			if (booleanTweets.get(i) == true) {
				fluStates.add(closestState.get(i));				
			}
		}*/
		
	public void printfluStates(){
		Set<String> distinct = new TreeSet<>(fluStates);
        for (String s: distinct) {
            System.out.println(s + ": " + Collections.frequency(fluStates, s));
        }	
	}
		
	/*Set<String> distinct = new TreeSet<>(fluStates);
        for (String s: distinct) {
            System.out.println(s + ": " + Collections.frequency(fluStates, s));
        }*/			
		
}
	
