package edu.upenn.cit594.processor;
import edu.upenn.cit594.datamanagement.*;

public class ReaderPicker {

	//based on format of tweets file, decide either txt or json reader
	public MainReader getReader(String fileFormat, String tweetsFileName) {
		if (fileFormat.equals("json")) {
			return new JSONReader(tweetsFileName);
		} else {
			return new TXTReader(tweetsFileName);
		}
	}
	//read the csv for all the state coordinates
	public MainReader getCSVReader(String statesFileName) {
		return new CSVReader(statesFileName);
	}
	
}
