package edu.upenn.cit594;
import java.io.File;
import java.io.IOException;
import edu.upenn.cit594.logging.Loggers;
import edu.upenn.cit594.processor.*;
import edu.upenn.cit594.ui.CommandLineUserInterface;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		if (args.length != 4) {
			System.out.println("Runtime Argument Number Error");
			System.exit(0);
		}
		
		String format = args[0];
		if (!format.equals("txt") && !format.equals("json")) {
			System.out.println("Parking File Format Error");
			System.exit(0);
		}
		
		String fileName = args[1];
		String statesName = args[2];
		String logFile = args[3];
		
		
		
		File logFiler = new File(logFile);
		if(!logFiler.exists()) {
			logFiler.createNewFile();
		} else {
			System.out.println("Log file already exists");
		}
		
		Loggers newLog = new Loggers(logFile);
		newLog.getInstance(logFile);
		
		Processor processor = new Processor(format, fileName, statesName, logFile);
		CommandLineUserInterface ui = new CommandLineUserInterface(processor);
		ui.displayValue();
		
	}

}
