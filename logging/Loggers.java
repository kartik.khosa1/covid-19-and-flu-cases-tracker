package edu.upenn.cit594.logging;

import java.io.File;
import java.io.PrintWriter;


public class Loggers {
	
	private PrintWriter output;
	private static Loggers instance = null;
	
	public Loggers(String logFile) {
		try {
			output = new PrintWriter(new File(logFile)); 
		}
		catch (Exception e){			
		}
	}	
	
	public static Loggers getInstance() {
		return instance;
	}
	
	public Loggers getInstance(String logFile) {
		if (instance == null) {
			instance = new Loggers(logFile);
		}
		return instance;
	}
	
	public void log(String msg){
		output.println(msg);
		output.flush();
	}
	
	


}
