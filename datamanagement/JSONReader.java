package edu.upenn.cit594.datamanagement;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;

import edu.upenn.cit594.data.Tweets;

public class JSONReader extends MainReader{

	protected String jsonFilename;
	
	public JSONReader(String fileName) {
		this.jsonFilename = fileName;
	}
	
	@Override
	public List<Tweets> read() {
		List<Tweets> allTweets = new ArrayList<>();
		JSONParser parser = new JSONParser();
		
		try {
			JSONArray obj  = (JSONArray) parser.parse(new FileReader(jsonFilename));
			//Iterator<?> iter = obj.iterator();
			for (int j = 0; j < obj.size(); j++) {
				JSONObject object = (JSONObject) obj.get(j);
				String location = (String) object.get("location").toString();
				String text = (String) object.get("text");
				//String time = (Date) object.get("time");				
				allTweets.add(new Tweets(location, text));

			}
		} catch (Exception e) {
			throw new IllegalStateException("File Not Found Or Cannot Read");
		}
		return allTweets;
	}
}