package edu.upenn.cit594.datamanagement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import edu.upenn.cit594.data.*;


public class CSVReader extends MainReader{
	
	protected String CSVFileName;
	
	public CSVReader(String CSVFileName) {
		this.CSVFileName = CSVFileName;
	}
	
	public List<csvStates> read() {
		List<csvStates> states = new ArrayList<>();
	
		String line = "";
		  
	
		try  {    
			BufferedReader br = new BufferedReader(new FileReader(CSVFileName));
			String linesplit = ",";
			while ((line = br.readLine()) != null) {  
				String[] splitter = line.split(linesplit);
				String state = splitter[0];
				String lat = splitter[1];
				String longi = splitter[2];
				states.add(new csvStates(lat, longi, state));  
			} 
			br.close();
		}   
		catch (IOException e)  {  
			e.printStackTrace();  
		}  		
		return states;
}
}
	