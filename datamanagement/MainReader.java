package edu.upenn.cit594.datamanagement;

public abstract class MainReader {
	
	public abstract Object read();

}
