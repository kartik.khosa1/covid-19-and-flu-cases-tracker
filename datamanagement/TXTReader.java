package edu.upenn.cit594.datamanagement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

import edu.upenn.cit594.data.Tweets;

public class TXTReader extends MainReader{

	protected String txtFileName;
	String identifier;
	String time;
	
	public TXTReader(String fileName) {
		this.txtFileName = fileName;
	}	
	
		@Override
		public List<Tweets> read() {
			List<Tweets> allTweets = new ArrayList<>();
			try {
				//int i = 1;
				String line = "";
				String linesplit = "\t";
				BufferedReader inputfile = new BufferedReader(new FileReader(txtFileName)); 
				while ((line = inputfile.readLine()) != null) {
					String[] temp = line.split(linesplit);
					String location = temp[0];
					String text = temp[3];

					allTweets.add(new Tweets(location, text));
					//i++;
				}
				inputfile.close();
			}
			catch (Exception e) {
				throw new IllegalStateException(e);
			}
			
			return allTweets;
		}
}

