package edu.upenn.cit594.ui;


import edu.upenn.cit594.processor.*;
public class CommandLineUserInterface {	
	
	private Processor processor;
	
	public CommandLineUserInterface(Processor processor) {
		this.processor = processor;
		
		processor.getLocationparsed();
		processor.getTextparsed();
		processor.getlatitudeparsed();
		processor.getlongitudeparsed();
		processor.getstateparsed();
		processor.getclosestState();
		processor.getbooleanTweets();
		processor.getfluStates();
		
	}
	
	public void displayValue() {
		processor.printfluStates();
	}
		
}
	
		

