package edu.upenn.cit594.data;

public class Tweets {
	
	String location;
	String text;
	String time;
	
	public Tweets(String location, String text) {
		this.location = location;
		this.text = text;
	}
	
	public String getLocation() {
		return location;
	}
	
	public String getText() {
		return text;
	}
	

}
