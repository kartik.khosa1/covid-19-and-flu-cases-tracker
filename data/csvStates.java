package edu.upenn.cit594.data;

public class csvStates {
	
	String lat;
	String longi;
	String state;	
	
	public csvStates(String lat, String longi, String state) {
		this.lat = lat;
		this.longi = longi;
		this.state = state;
	}
	
	public String getLat() {
		return lat;
	}
	
	public String getLongi() {
		return longi;
	}
	
	public String getState() {
		return state;
	}

}
